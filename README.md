# Biscuit-Quality-Control


This project presents a Visual Quality Inspection system using Computer Vision that detects the defected biscuits and rejects them.

In addition, we are looking in our project to detect the accuracy rate of a such system and the added value of the latter compared to manual processing.

It's part of our university projects with my Teammates:
* TAGHOUTI Oumaima
* Hammouda Ala Eddine
* ELJ Sabri

Demonstration tests :  https://www.youtube.com/watch?v=NXm8iuDB9wk&
